# TP3 : Progressons vers le réseau d'infrastructure
- [TP3 : Progressons vers le réseau d'infrastructure](#tp3--progressons-vers-le-réseau-dinfrastructure)
- [1. Routeur](#1-routeur)
- [2. Serveur DHCP](#2-serveur-dhcp)
  - [🖥️ VM marcel.client1.tp3](#️-vm-marcelclient1tp3)
- [3. Serveur DNS](#3-serveur-dns)
  - [🖥️ VM dns1.server1.tp3](#️-vm-dns1server1tp3)
- [4. Get deeper](#4-get-deeper)
  - [🖥️ VM johnny.client1.tp3](#️-vm-johnnyclient1tp3)
- [5. Serveur Web](#5-serveur-web)
  - [🖥️ VM web1.server2.tp3](#️-vm-web1server2tp3)
- [6. Le setup wola](#6-le-setup-wola)
  - [🖥️ VM nfs1.server2.tp3](#️-vm-nfs1server2tp3)
- [7. Un peu de théorie : TCP et UDP](#7-un-peu-de-théorie--tcp-et-udp)
- [8. El final](#8-el-final)
  - [Tableau des réseaux + adressage](#tableau-des-réseaux--adressage)
  - [Schema du réseau](#schema-du-réseau)



# 1. Routeur
🖥️ VM router.tp3
🌞 Vous pouvez d'ores-et-déjà créer le routeur. Pour celui-ci, vous me prouverez que :

* il a bien une IP dans les 3 réseaux, l'IP que vous avez choisie comme IP de passerelle
```
[maxime@routeur ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:4f:97:53 brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
       valid_lft 85812sec preferred_lft 85812sec
    inet6 fe80::a00:27ff:fe4f:9753/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:25:f3:c0 brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.126/25 brd 10.3.1.127 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe25:f3c0/64 scope link
       valid_lft forever preferred_lft forever
4: enp0s9: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:37:28:bf brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.206/25 brd 10.3.1.255 scope global noprefixroute enp0s9
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe37:28bf/64 scope link
       valid_lft forever preferred_lft forever
5: enp0s10: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:87:87:d2 brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.190/26 brd 10.3.1.191 scope global noprefixroute enp0s10
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe87:87d2/64 scope link
       valid_lft forev
```
`On peut voir que les cartes enp0s8/9 et 10 , on bien chacun les bonnes adresse de passerelle `
```
* il a un accès internet
On ping par exemple l'adresse de google pour voir si on a internet
[maxime@routeur ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=114 time=23.9 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=114 time=191 ms
```
* il a de la résolution de noms
```
Grace à la commande dig (nom de domaine) , on peut faire de la resolution de nom 
[maxime@routeur ~]$ dig ynov.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> ynov.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 45286
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4000
;; QUESTION SECTION:
;ynov.com.                      IN      A

;; ANSWER SECTION:
ynov.com.               2287    IN      A       92.243.16.143

;; Query time: 26 msec
;; SERVER: 10.33.10.2#53(10.33.10.2)
;; WHEN: Wed Sep 29 15:46:50 CEST 2021
;; MSG SIZE  rcvd: 53
```
* il porte le nom router.tp3*
En faisant hostname , on obtient le nom de la machine.
```
[maxime@routeur ~]$ hostname
routeur.tp3
```
* n'oubliez pas d'activer le routage sur la machine
```
On active le routage de la vm grâce a :
$ sudo firewall-cmd --list-all
$ sudo firewall-cmd --get-active-zone

# Activation du masquerading
$ sudo firewall-cmd --add-masquerade --zone=public
$ sudo firewall-cmd --add-masquerade --zone=public --permanent
```
# 2. Serveur DHCP
**🌞 Mettre en place une machine qui fera office de serveur DHCP dans le réseau client1. Elle devra :**

* porter le nom dhcp.client1.tp3
```
[maxime@dhcp ~]$ hostname
dhcp.client1.tp3
[maxime@dhcp ~]$
```
* donner une IP aux machines clients qui le demande
```

On peut voir que mon client 1 à bien reçu son adresse IP via le dhcp

[maxime@client1 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:4f:97:53 brd ff:ff:ff:ff:ff:ff
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:e3:95:a2 brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.131/26 brd 10.3.1.191 scope global dynamic noprefixroute enp0s8
       valid_lft 762sec preferred_lft 762sec
    inet6 fe80::a00:27ff:fee3:95a2/64 scope link
       valid_lft forever preferred_lft forever
[maxime@client1 ~]
```
* leur donner l'adresse de leur passerelle
```
Grace au ip a effectué juste avant, on peut remarquer je le client 1 à bien reçu son adresse de passerelle car sont brd est à 10.3.1.191 sinon avec un ip r s : 
[maxime@client1 ~]$ ip r s
default via 10.3.1.190 dev enp0s8 proto dhcp metric 100
10.3.1.128/26 dev enp0s8 proto kernel scope link src 10.3.1.131 metric 100
[maxime@client1 ~]$
```
* leur donner l'adresse d'un DNS utilisable
```
On remarque quand faisant un dig (nom de domaine), le serveur dns marche 
[maxime@client1 ~]$ dig ynov.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> ynov.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 25215
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;ynov.com.                      IN      A

;; ANSWER SECTION:
ynov.com.               9817    IN      A       92.243.16.143

;; Query time: 28 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Wed Sep 29 16:59:16 CEST 2021
;; MSG SIZE  rcvd: 53

[maxime@client1 ~]$
```
**📁 Fichier dhcpd.conf**


## 🖥️ VM marcel.client1.tp3
**🌞 Mettre en place un client dans le réseau client1**

* de son p'tit nom marcel.client1.tp3
```
[maxime@marcel ~]$ hostname
marcel.client1.tp3
[maxime@marcel ~]$
```
📝checklist📝
la machine récupérera une IP dynamiquement grâce au serveur DHCP
```
Le serveur recupere bien une adresse ip dynamique 

[maxime@marcel ~]$ ip  a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:4f:97:53 brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic enp0s3
       valid_lft 86386sec preferred_lft 86386sec
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:e3:95:a2 brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.131/26 brd 10.3.1.191 scope global dynamic enp0s8
       valid_lft 888sec preferred_lft 888sec
[maxime@marcel ~]$
```
ainsi que sa passerelle et une adresse d'un DNS utilisable
```
On peut voir que la passerelle et le dns marche bien car le routeur et le dhcp, on pu fournir les informations demandé 

[maxime@marcel ~]$ dig youtube.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> youtube.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 41566
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;youtube.com.                   IN      A

;; ANSWER SECTION:
youtube.com.            234     IN      A       216.58.214.78

;; Query time: 50 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Thu Sep 30 14:37:21 CEST 2021
;; MSG SIZE  rcvd: 56


[maxime@marcel ~]$ ip r s
default via 10.3.1.190 dev enp0s8 proto dhcp metric 100
10.3.1.128/26 dev enp0s8 proto kernel scope link src 10.3.1.131 metric 100
[maxime@marcel ~]$
```

**🌞 Depuis marcel.client1.tp3**

* prouver qu'il a un accès internet + résolution de noms, avec des infos récupérées par votre DHCP
```
Ping : [maxime@marcel ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=19.5 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=113 time=22.4 ms

Resolution de nom : [maxime@marcel ~]$ dig ynoc.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> ynoc.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 28571
;; flags: qr rd ra ad; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;ynoc.com.                      IN      A

;; ANSWER SECTION:
ynoc.com.               300     IN      A       150.95.8.223

;; Query time: 27 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Thu Sep 30 14:40:20 CEST 2021
;; MSG SIZE  rcvd: 53

[maxime@marcel ~]$
```

* à l'aide de la commande traceroute, prouver que marcel.client1.tp3 passe par router.tp3 pour sortir de son réseau
```
Grace à trace route on peut voir que marcel passe bien  par la passerelle 

[maxime@marcel ~]$ sudo traceroute  google.com
traceroute to google.com (216.58.215.46), 30 hops max, 60 byte packets
 1  _gateway (10.3.1.190)  0.723 ms  0.684 ms  0.662 ms
 2  10.0.2.2 (10.0.2.2)  0.866 ms  0.843 ms  0.823 ms
 3  * * *
 4  * * *
 [etc....]
29  * * *
30  * * *
[maxime@marcel ~]$
```
# 3. Serveur DNS
## 🖥️ VM dns1.server1.tp3

**🌞 Mettre en place une machine qui fera office de serveur DNS**

```
# dans le réseau `server1`
## affichage de la carte enp0s8 -> voir le tableau des reseaux ( lien au dessus )
[targa@dns1 ~]$ ip a
[...]
2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:3d:f1:da brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.11/25 brd 10.3.1.127 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe3d:f1da/64 scope link
       valid_lft forever preferred_lft forever
# de son p'tit nom `dns1.server1.tp3`
## Définition du nom de domaine
[targa@dns1 ~]$ sudo hostname dns1.server1.tp3
[targa@dns1 ~]$ echo "dns1.server1.tp3" | sudo tee /etc/hostname
dns1.server1.tp3
## vérification 
[targa@dns1 ~]$ hostname
dns1.server1.tp3

# il faudra lui ajouter un serveur DNS public connu, afin qu'il soit capable de résoudre des noms publics comme `google.com`
[targa@dns1 ~]$ cat /etc/resolv.conf
search server1.tp3
nameserver 8.8.8.8

#installation du serveur dns 
[targa@dns1 ~]$ sudo dnf install -y bind bind-utils
[...]


```


```
# lancement du service
[targa@dns1 ~]$ sudo systemctl enable named
Created symlink /etc/systemd/system/multi-user.target.wants/named.service → /usr/lib/systemd/system/named.service.
[targa@dns1 ~]$ sudo systemctl start named

# ajout du service dns sur le firewall
[targa@dns1 ~]$ sudo firewall-cmd --add-service=dns --permanent
success
[targa@dns1 ~]$ sudo firewall-cmd --reload
success
[targa@dns1 ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s8
  sources:
  services: dns ssh
  ports: 53/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:

```

🌞 **Tester le DNS depuis `marcel.client1.tp3`**

```
#Tester le DNS depuis marcel.client1.tp3
## Affichage du serveur DNS utilisée
[targa@marcel etc]$ cat /etc/resolv.conf
nameserver 10.3.1.11
#test
[targa@marcel etc]$ dig dns1.server1.tp3
; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> dns1.server1.tp3
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 58935
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 1, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: be3d55b502ef4eb66a596e9c616b8f33eb9d31aad8b560ca (good)
;; QUESTION SECTION:
;dns1.server1.tp3.              IN      A

;; ANSWER SECTION:
dns1.server1.tp3.       86400   IN      A       10.3.1.11

;; AUTHORITY SECTION:
server1.tp3.            86400   IN      NS      dns1.server1.tp3.

;; Query time: 0 msec
;; SERVER: 10.3.1.11#53(10.3.1.11)
;; WHEN: Sun Oct 17 04:49:08 CEST 2021
;; MSG SIZE  rcvd: 103
```

```
# Preuve que c'est bien le dns local (voir tableau d'adressage pour la preuve que c'est la bonne ip )
[...]
;; SERVER: 10.3.1.11#53(10.3.1.11)
[...]
```


🌞 Configurez l'utilisation du serveur DNS sur TOUS vos noeuds

```
# les serveurs, on le fait à la main
[targa@dhcp ~]$ cat /etc/resolv.conf
search server1.tp3
nameserver 10.3.1.11

# les clients, c'est fait via DHCP
[root@dhcp ~]# vim /etc/dhcp/dhcpd.conf
 option domain-name-servers 10.3.1.11 ;

```
# 4. Get deeper

**🌞 Affiner la configuration du DNS**

faites en sorte que votre DNS soit désormais aussi un forwarder DNS
c'est à dire que s'il ne connaît pas un nom, il ira poser la question à quelqu'un d'autre


Hint : c'est la clause recursion dans le fichier /etc/named.conf. Et c'est déjà activé par défaut en fait.

**🌞 Test !**

vérifier depuis marcel.client1.tp3 que vous pouvez résoudre des noms publics comme google.com en utilisant votre propre serveur DNS (commande dig)
pour que ça fonctionne, il faut que dns1.server1.tp3 soit lui-même capable de résoudre des noms, avec 1.1.1.1 par exemple

```
[maxime@marcel ~]$ dig 1.1.1.1

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> 1.1.1.1
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: REFUSED, id: 19454
;; flags: qr rd; QUERY: 1, ANSWER: 1, AUTHORITY: 1, ADDITIONAL: 1
;; WARNING: recursion requested but not available

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: 100b912af13372be7498c166615c5ff9f0ef22a1e49ea9b2 (good)
;; QUESTION SECTION:
;1.1.1.1.                       IN      A

;; Query time: 0 msec
;; SERVER: 10.3.1.2#53(10.3.1.2)
;; WHEN: Tue Oct 05 16:23:53 CEST 2021
;; MSG SIZE  rcvd: 64

<!-----------------------Exemple 2  --------------------->

[maxime@marcel ~]$ dig amazon.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> amazon.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: REFUSED, id: 42051
;; flags: qr rd; QUERY: 1, ANSWER: 1, AUTHORITY: 1, ADDITIONAL: 1
;; WARNING: recursion requested but not available

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: e39bac980cbb280cee61b430615c600654d98e4be4e9039e (good)
;; QUESTION SECTION:
;amazon.com.                    IN      A

;; Query time: 0 msec
;; SERVER: 10.3.1.2#53(10.3.1.2)
;; WHEN: Tue Oct 05 16:24:06 CEST 2021
;; MSG SIZE  rcvd: 67

[maxime@marcel ~]$
```
## 🖥️ VM johnny.client1.tp3
**🌞 Affiner la configuration du DHCP**

faites en sorte que votre DHCP donne désormais l'adresse de votre serveur DNS aux clients
créer un nouveau client johnny.client1.tp3 qui récupère son IP, et toutes les nouvelles infos, en DHCP

```
La machine johnny recupere bien les infos du dhcp et du dns 

[maxime@johnny ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:4f:97:53 brd ff:ff:ff:ff:ff:ff
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:ed:00:9d brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.132/26 brd 10.3.1.191 scope global dynamic noprefixroute enp0s8
       valid_lft 891sec preferred_lft 891sec
    inet6 fe80::a00:27ff:feed:9d/64 scope link
       valid_lft forever preferred_lft forever

       
[maxime@johnny ~]$ dig youtube.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> youtube.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: REFUSED, id: 7348
;; flags: qr rd; QUERY: 1, ANSWER: 0, AUTHORITY: 0, ADDITIONAL: 1
;; WARNING: recursion requested but not available

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: 27e1a4b302d14c3cbbb7ceae615c61c777eb37d363cf29a5 (good)
;; QUESTION SECTION:
;youtube.com.                   IN      A

;; Query time: 1 msec
;; SERVER: 10.3.1.2#53(10.3.1.2)
;; WHEN: Tue Oct 05 16:45:49 CEST 2021
;; MSG SIZE  rcvd: 68

[maxime@johnny ~]$
```
# 5. Serveur Web
## 🖥️ VM web1.server2.tp3

**🌞 Setup d'une nouvelle machine, qui sera un serveur Web, une belle appli pour nos clients**

réseau server2

hello web1.server2.tp3 !
📝checklist📝
vous utiliserez le serveur web que vous voudrez, le but c'est d'avoir un serveur web fast, pas d'y passer 1000 ans :)

réutilisez votre serveur Web du TP1 Linux

ou montez un bête NGINX avec la page d'accueil (ça se limite à un dnf install puis systemctl start nginx)
ou ce que vous voulez, du moment que c'est fast

dans tous les cas, n'oubliez pas d'ouvrir le port associé dans le firewall pour que le serveur web soit joignable



**🌞 Test test test et re-test**

testez que votre serveur web est accessible depuis marcel.client1.tp3

utilisez la commande curl pour effectuer des requêtes HTTP
```
Grace au curl , on peut voir que la connexion marche bien entre marcel et le serveur web 

[maxime@marcel ~]$ curl 10.3.1.194:8888
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Directory listing for /</title>
</head>
<body>
<h1>Directory listing for /</h1>
<hr>
<ul>
<li><a href="bin/">bin@</a></li>
<li><a href="boot/">boot/</a></li>
<li><a href="dev/">dev/</a></li>
<li><a href="etc/">etc/</a></li>
<li><a href="home/">home/</a></li>
<li><a href="lib/">lib@</a></li>
<li><a href="lib64/">lib64@</a></li>
<li><a href="media/">media/</a></li>
<li><a href="mnt/">mnt/</a></li>
<li><a href="opt/">opt/</a></li>
<li><a href="proc/">proc/</a></li>
<li><a href="root/">root/</a></li>
<li><a href="run/">run/</a></li>
<li><a href="sbin/">sbin@</a></li>
<li><a href="srv/">srv/</a></li>
<li><a href="sys/">sys/</a></li>
<li><a href="tmp/">tmp/</a></li>
<li><a href="usr/">usr/</a></li>
<li><a href="var/">var/</a></li>
</ul>
<hr>
</body>
</html>
[maxime@marcel ~]$
```
# 6. Le setup wola
## 🖥️ VM nfs1.server2.tp3
🌞 Setup d'une nouvelle machine, qui sera un serveur NFS

réseau server2

son nooooom : nfs1.server2.tp3 !
📝checklist📝
je crois que vous commencez à connaître la chanson... Google "nfs server rocky linux"

ce lien me semble être particulièrement simple et concis


vous partagerez un dossier créé à cet effet : /srv/nfs_share/
```
On peut voir que le partage de fchier est bon du côté nfs 

[maxime@nsf1 ~]$ sudo mkdir /srv/nfs_share/
[maxime@nsf1 ~]$ systemctl enable --now rpcbind nfs-server
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-unit-files ====
Authentication is required to manage system service or unit files.
Authenticating as: greg (maxime)
Password:
==== AUTHENTICATION COMPLETE ====
Created symlink /etc/systemd/system/multi-user.target.wants/nfs-server.service → /usr/lib/systemd/system/nfs-server.service.
==== AUTHENTICATING FOR org.freedesktop.systemd1.reload-daemon ====
Authentication is required to reload the systemd state.
Authenticating as: greg (maxime)
Password:
==== AUTHENTICATION COMPLETE ====
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-units ====
Authentication is required to start 'rpcbind.service'.
Authenticating as: greg (maxime)
Password:
==== AUTHENTICATION COMPLETE ====
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-units ====
Authentication is required to start 'nfs-server.service'.
Authenticating as: greg (maxime)
Password:
==== AUTHENTICATION COMPLETE ====
[maxime@nsf1 ~]$ firewall-cmd --add-service=nfs
Authorization failed.
    Make sure polkit agent is running or run the application as superuser.
[maxime@nsf1 ~]$ sudo firewall-cmd --add-service=nfs
success
[maxime@nsf1 ~]$ sudo firewall-cmd --add-service=nfs  --permanent
success
```
🌞 Configuration du client NFS

effectuez de la configuration sur web1.server2.tp3 pour qu'il accède au partage NFS
le partage NFS devra être monté dans /srv/nfs/
sur le même site, y'a ça
```
Le montage sur le serveur web , c'est bien fait sur srv/nfs .

[maxime@web1 ~]$ sudo mount -t nfs 10.3.1.195:/srv/nfs_share/ /srv/nfs
[maxime@web1 ~]$ df -hT
Filesystem                Type      Size  Used Avail Use% Mounted on
devtmpfs                  devtmpfs  891M     0  891M   0% /dev
tmpfs                     tmpfs     909M     0  909M   0% /dev/shm
tmpfs                     tmpfs     909M  8.6M  901M   1% /run
tmpfs                     tmpfs     909M     0  909M   0% /sys/fs/cgroup
/dev/mapper/rl-root       xfs       6.2G  2.1G  4.1G  34% /
/dev/sda1                 xfs      1014M  301M  714M  30% /boot
tmpfs                     tmpfs     182M     0  182M   0% /run/user/1000
10.3.1.195:/srv/nfs_share nfs4      6.2G  2.1G  4.1G  34% /srv/nfs
[maxime@web1 ~]$

```

🌞 TEEEEST

tester que vous pouvez lire et écrire dans le dossier /srv/nfs depuis web1.server2.tp3

```
Point de vu du web 1 : 

[maxime@web1 ~]$ sudo vi /srv/nfs/test.nfs
[sudo] password for maxime:
[maxime@web1 ~]$ [maxime@web1 ~]$ sudo vi /srv/nfs/test_nfs
[maxime@web1 ~]$ sudo ls /srv/nfs/test_nfs
/srv/nfs/test_nfs
[maxime@web1 ~]$
```

vous devriez voir les modifications du côté de  nfs1.server2.tp3 dans le dossier /srv/nfs_share/
```
Point de vu de nfs1 : 
[maxime@nsf1 ~]$ [maxime@nsf1 ~]$ sudo ls /srv/nfs_share/
test_nfs
[maxime@nsf1 ~]$
```

# 7. Un peu de théorie : TCP et UDP
Bon bah avec tous ces services, on a de la matière pour bosser sur TCP et UDP. P'tite partie technique pure avant de conclure.
🌞 Déterminer, pour chacun de ces protocoles, s'ils sont encapsulés dans du TCP ou de l'UDP :

SSH = TCP

HTTP = TCP

DNS = UDP

NFS= TCP

**📁 Captures réseau tp3_ssh.pcap, tp3_http.pcap, tp3_dns.pcap et tp3_nfs.pcap**

Prenez le temps de réfléchir à pourquoi on a utilisé TCP ou UDP pour transmettre tel ou tel protocole. Réfléchissez à quoi servent chacun de ces protocoles, et de ce qu'on a besoin qu'ils réalisent.

🌞 Capturez et mettez en évidence un 3-way handshake
📁 Capture réseau tp3_3way.pcap

# 8. El final
🌞 Bah j'veux un schéma.

réalisé avec l'outil de votre choix
un schéma clair qui représente

les réseaux

les adresses de réseau devront être visibles


toutes les machines, avec leurs noms
devront figurer les IPs de toutes les interfaces réseau du schéma
pour les serveurs : une indication de quel port est ouvert


vous représenterez les host-only comme des switches
dans le rendu, mettez moi ici à la fin :

le schéma
le 🗃️ tableau des réseaux 🗃️
le 🗃️ tableau d'adressage 🗃️

on appelle ça aussi un "plan d'adressage IP" :)

J'vous le dis direct, un schéma moche avec Paint c'est -5 Points. Je vous recommande draw.io.

🌞 Et j'veux des fichiers aussi, tous les fichiers de conf du DNS

📁 Fichiers de zone

📁 Fichier de conf principal DNS named.conf

faites ça à peu près propre dans le rendu, que j'ai plus qu'à cliquer pour arriver sur le fichier ce serait top

## Tableau des réseaux + adressage
```
| Nom du réseau | Adresse du réseau | Masque            | Nombre de clients possibles | Adresse passerelle | Adresse broadcast |
| ------------- | ----------------- | ----------------- | --------------------------- | ------------------ | ----------------- |
| `server1`     | `10.3.1.0`        | `255.255.255.128` | `126`                       | `10.3.1.126`       | `10.3.1.127`      |
| `client1`     | `10.3.1.128`      | `255.255.255.192` | `62`                        | `10.3.1.190`       | `10.3.1.191`      |
| `server2`     | `10.3.1.192`      | `255.255.255.240` | `14`                        | `10.3.1.206`       | `10.3.1.207`      |



| Nom machine          | Adresse IP `client1` | Adresse IP `server1` | Adresse IP `server2` | Adresse de passerelle |
| -------------------- | -------------------- | -------------------- | -------------------- | --------------------- |
| `router.tp3`         | `10.3.1.190/26`      | `10.3.1.126/25`      | `10.3.1.206/28`      | Carte NAT             |
| `dhcp.client1.tp3`   | `10.3.1.130/26`      | x                    | x                    | x                     |
| `dns1.server1.tp3`   | x                    | `10.3.1.2/25`        | x                    | x                     |
| `marcel.client1.tp3` | `10.3.1.131/26`      |                      | x                    | x                     |
| `johnny.client1.tp3` | `10.3.1.132/26`      |                      | x                    | x                     |
| `web1.server2.tp3`   | x                    |                      | `10.3.1.194/28`      | x                     |
| `nfs1.server2.tp3`   | x                    |                      | `10.3.1.195/28`      | x                     |
```

## Schema du réseau
 
![sparkles](schema%20reseau.png)
