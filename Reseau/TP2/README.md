# B2_TP2_Réseau

## I. ARP

### 1. Echange ARP

#### 🌞Générer des requêtes ARP

- Effectuer un ping d'une machine à l'autre

Depuis node1:
```
[maxime@node1 ~]$ ping 10.2.1.11
PING 10.2.1.11 (10.2.1.11) 56(84) bytes of data.
64 bytes from 10.2.1.11: icmp_seq=1 ttl=64 time=0.719 ms
64 bytes from 10.2.1.11: icmp_seq=2 ttl=64 time=1.74 ms
64 bytes from 10.2.1.11: icmp_seq=3 ttl=64 time=4.11 ms
```
```
Depuis node2:

[maxime@node2 ~]$ ping 10.2.1.12
PING 10.2.1.12 (10.2.1.12) 56(84) bytes of data.
64 bytes from 10.2.1.12: icmp_seq=1 ttl=64 time=1.22 ms
64 bytes from 10.2.1.12: icmp_seq=2 ttl=64 time=1.91 ms
```

- Observer les tables ARP des deux machines
```
Table ARP de node1:

[maxime@node1 ~]$ ip n s
10.0.2.2 dev enp0s3 lladdr 52:54:00:12:35:02 STALE
10.2.1.12 dev enp0s8 lladdr 08:00:27:03:f6:76 STALE
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:0f REACHABLE
Table ARP de node2:

[maxime@node2 ~]$ ip n s
10.0.2.2 dev enp0s3 lladdr 52:54:00:12:35:02 STALE
10.2.1.11 dev enp0s8 lladdr 08:00:27:20:eb:07 STALE
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:0f DELAY
```

- Repérer l'adresse MAC de node1 dans la table ARP de node2 et vice-versa
```
D'apres les tables ARP juste au dessus, on peut voir que l'adresse mac de node1 est: 08:00:27:20:eb:07 et celle de node2 est: 08:00:27:03:f6:76
```
- Prouvez que l'info est correcte (que l'adresse MAC que vous voyez dans la table est bien celle de la machine correspondante)
```
Si on fait un "ip a" depuis node1, on peut voir que son adresse mac est bien celle vue dans la table arp de node2 et inversement:

node1:

[maxime@node1 ~]$ ip a
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:20:eb:07 brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.11/24 brd 10.2.1.255 scope global noprefixroute enp0s8



node2:

[greg@node2 ~]$ ip a
[...]
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:03:f6:76 brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.12/24 brd 10.2.1.255 scope global noprefixroute enp0s8
[...]
```

### 2. Analyse de trames

#### 🌞Analyse de trames

- Utilisez la commande tcpdump pour réaliser une capture de trame
Pour utiliser tcpdump , il faut faire : (exclusion du port 22 pour ne pas recevoir les trames permanantes de la connexion ssh)
```
[maxime@node1 ~]$ sudo tcpdump -i enp0s8 -c 10 -w tp2_arp.pcap not port 22
dropped privs to tcpdump
tcpdump: listening on enp0s8, link-type EN10MB (Ethernet), capture size 262144 bytes
10 packets captured
10 packets received by filter
0 packets dropped by kernel
```
* videz vos tables ARP, sur les deux machines, puis effectuez un ping
```
vider la table ARP:
[maxime@node1 ~]$ sudo ip neigh flush all
[sudo] password for maxime:
[maxime@node1 ~]$ 
```
* stoppez la capture, et exportez-la sur votre hôte pour visualiser avec Wireshark
```
Dans la commande tcpdump utilisé juste avant , la partie " - c 10" permet de récuperer 10 trames et de stopper la capture juste apres .

Pour exporter, il faut utiliser scp pour le tranfert de fichier en ssh

PS C:\WINDOWS\system32> scp maxime@10.2.1.11:/home/greg/tp2_arp.pcap ./
maxime@10.2.1.11's password:
tp2_arp.pcap                                                          100% 3234     1.6MB/s   00:00
PS C:\WINDOWS\system32>
```
* mettez en évidence les trames ARP
```
(Voir screen 1) on peut voir en évidance les trames ARP en jaune 
```
* écrivez, dans l'ordre, les échanges ARP qui ont eu lieu, je veux TOUTES les trames
* uniquement l'ARP
    sans doublons de trame, dans le cas où vous avez des trames en double

| ordre | type trame  | source                      | destination                |
|-------|-------------|--------------------------   |----------------------------|
| 1     | Requête ARP | `node1` `08:00:27:20:eb:07` | Broadcast `FF:FF:FF:FF:FF` |
| 2     | Réponse ARP | `node2` `08:00:27:03:f6:76` | `node1` `08:00:27:20:eb:07`|
| 3     | Requête ARP | `node2` `08:00:27:03:f6:76` | `node1` `08:00:27:20:eb:07`|   
| 4     | Réponse ARP | `node1` `08:00:27:20:eb:07` | `node2` `08:00:27:03:f6:76`|

## II. Routage

1. Mise en place du routage
**🌞Activer le routage sur le noeud router.net2.tp2**

```
Activation du routage
Pour activer le routage, il faut simplement autoriser la machine à traiter des paquets IP qui ne lui sont pas destinés (normal, ils sont destinés pour un autre réseau, puisqu'on parle d'un routeur :) ).
Pour ce faire :
# On repère la zone utilisée par firewalld, généralement 'public' si vous n'avez pas fait de conf spécifique
$ sudo firewall-cmd --list-all
$ sudo firewall-cmd --get-active-zone

# Activation du masquerading
$ sudo firewall-cmd --add-masquerade --zone=public
$ sudo firewall-cmd --add-masquerade --zone=public--permanent
```
**🌞Ajouter les routes statiques nécessaires pour que node1.net1.tp2 et marcel.net2.tp2 puissent se ping**

```
Node 1 : 
[maxime@node1 network-scripts]$ ip r s
10.2.1.0/24 dev enp0s8 proto kernel scope link src 10.2.1.11 metric 100
10.2.2.0/24 via 10.2.1.254 dev enp0s8 proto static metric 100
[maxime@node1 network-scripts]$ ping 10.2.2.12
PING 10.2.2.12 (10.2.2.12) 56(84) bytes of data.
64 bytes from 10.2.2.12: icmp_seq=1 ttl=63 time=1.02 ms
64 bytes from 10.2.2.12: icmp_seq=2 ttl=63 time=0.692 ms
```
```
Node 2:
[maxime@marcel network-scripts]$ ip r s
10.2.1.0/24 via 10.2.2.254 dev enp0s8 proto static metric 100
10.2.2.0/24 dev enp0s8 proto kernel scope link src 10.2.2.12 metric 100
[maxime@marcel network-scripts]$ ping 10.2.1.11
PING 10.2.1.11 (10.2.1.11) 56(84) bytes of data.
64 bytes from 10.2.1.11: icmp_seq=1 ttl=63 time=0.733 ms
64 bytes from 10.2.1.11: icmp_seq=2 ttl=63 time=0.685 ms
```
2. Analyse de trames
**🌞Analyse des échanges ARP**

* videz les tables ARP des trois noeuds
```
Voici la commande pour vider les tables ARP :
"sudo ip neigh flush all"
```
* effectuez un ping de node1.net1.tp2 vers marcel.net2.tp2
```
[maxime@node1 ~]$ ping marcel
PING marcel (10.2.2.12) 56(84) bytes of data.
64 bytes from marcel (10.2.2.12): icmp_seq=1 ttl=63 time=0.855 ms
64 bytes from marcel (10.2.2.12): icmp_seq=2 ttl=63 time=1.57 ms
```
* regardez les tables ARP des trois noeuds
```
Node 1 : 
[maxime@node1 ~]$ ip neigh show
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:0f DELAY
10.2.1.254 dev enp0s8 lladdr 08:00:27:14:94:f7 STALE

Marcel:
[maxime@marcel ~]$ ip neigh show
10.2.2.1 dev enp0s8 lladdr 0a:00:27:00:00:08 DELAY
10.2.2.254 dev enp0s8 lladdr 08:00:27:37:88:f1 STALE

Routeur :
[maxime@routeur ~]$ ip neigh show
10.2.1.11 dev enp0s8 lladdr 08:00:27:58:80:da STALE
10.2.2.12 dev enp0s9 lladdr 08:00:27:3a:6d:54 STALE
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:0f DELAY
```
* essayez de déduire un peu les échanges ARP qui ont eu lieu
```
Node 1 est celui qui fait une requete de ping vers la premiere adresse routeur pour recuperer ladresse MAC de la machine . Ensuite le routeur va interpreter les requetes ping et ARP et voir vers ou il doit le redistribuer , sachant que le routeur possede 3 route , il sait qu'il doit transmettre la requete du ping vers marcel .. Une fois qu'il a reçu cette requete, il refait le chemon retour (marcel -> routeur -> node 1)
```
* répétez l'opération précédente (vider les tables, puis ping), en lançant tcpdump sur node1 et marcel, afin de capturer les échanges depuis les 2 points de vue
```
Pour vider : sudo ip neigh flush all 

Pour capturer : sudo tcpdump -i enp0s8 -w tp2_arp.pcap not port 22
```

* écrivez, dans l'ordre, les échanges ARP qui ont eu lieu, puis le ping et le pong, je veux TOUTES les trames utiles pour l'échange

| ordre | type trame  | IP source | MAC source                 | IP destination | MAC destination             |
|-------|-------------|-----------|----------------------------|----------------|-----------------------------|
| 1     | Requête ARP | x         |`node1` `08:00:27:20:eb:07` | x              |Broadcast `ff:ff:ff:ff:ff:ff`|
| 2     | Requête ARP | x         |`router` `08:00:27:d5:15:3d`| x              |Broadcast `ff:ff:ff:ff:ff:ff`|
| 3     | Réponse ARP | x         |`router` `08:00:27:bd:59:55`| x              |`node1` `08:00:27:20:eb:07`  |
| 4     | Réponse ARP | x         |`marcel` `08:00:27:03:f6:76`| x              |`router` `08:00:27:d5:15:3d` |
| 5     | Ping        | 10.2.1.11 |`node1` `08:00:27:20:eb:07` | 10.2.1.254     |`router` `08:00:27:bd:59:55` |
| 6     | Ping        | 10.2.2.254|`router` `08:00:27:d5:15:3d`| 10.2.2.12      |`marcel` `08:00:27:03:f6:76` |
| 7     | Pong        | 10.2.2.12 |`marcel` `08:00:27:03:f6:76`| 10.2.2.254     |`router` `08:00:27:d5:15:3d` |
| 8     | Pong        | 10.2.1.254|`router` `08:00:27:bd:59:55`| 10.2.1.11      |`node1` `08:00:27:20:eb:07`  |
## 3. Accès internet
**🌞Donnez un accès internet à vos machines**

* le routeur a déjà un accès internet
```
[maxime@routeur ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=114 time=20.5 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=114 time=19.9 ms
```
* ajoutez une route par défaut à node1.net1.tp2 et marcel.net2.tp2
```
Premierement, il faut rentrer dans la console (sur les deux machines): 
[maxime@node1 ~]$ sudo ip route add default via 10.2.1.254 dev enp0s8
[maxime@node1 ~]$ sudo nmcli con reload
[maxime@node1 ~]$ sudo nmcli con up enp0s8

Puis mettre dans /etc/sysconfig/network , la GATEWAY=10.2.0.254 pour accéder au NAT*
```

* vérifiez que vous avez accès internet avec un ping
 le ping doit être vers une IP, PAS un nom de domaine
 ```
 [maxime@marcel ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=19.2 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=113 time=19.1 ms
```
```
[maxime@node1 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=22.3 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=113 time=19.3 ms
```

* donnez leur aussi l'adresse d'un serveur DNS qu'ils peuvent utiliser
```
On configure le DNS grâce au fichier ""/etc/resolv.conf"
 et on marque dedans "nameserver 1.1.1.1"
```
 
* vérifiez que vous avez une résolution de noms qui fonctionne avec dig
```
[maxime@node1 ~]$ dig google.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 7194
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;google.com.                    IN      A

;; ANSWER SECTION:
google.com.             232     IN      A       216.58.209.238

;; Query time: 38 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Fri Sep 24 16:19:27 CEST 2021
;; MSG SIZE  rcvd: 55
```
* puis avec un ping vers un nom de domaine

```
[maxime@node1 ~]$ ping ynov.com
PING ynov.com (92.243.16.143) 56(84) bytes of data.
64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=1 ttl=52 time=18.6 ms
64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=2 ttl=52 time=19.6 ms
```

**🌞Analyse de trames**

* effectuez un ping 8.8.8.8 depuis node1.net1.tp2
```
[maxime@node1 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=20.4 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=113 time=20.5 ms
capturez le ping depuis node1.net1.tp2 avec tcpdump
```
```
[maxime@node1 ~]$ sudo tcpdump -i enp0s8 -c 10 -w tp2_arp.pcap not port 22
[sudo] password for maxime:
dropped privs to tcpdump
tcpdump: listening on enp0s8, link-type EN10MB (Ethernet), capture size 262144 bytes
10 packets captured
13 packets received by filter
0 packets dropped by kernel
```
* analysez un ping aller et le retour qui correspond et mettez dans un tableau : 

| ordre | type trame | IP source           | MAC source                  | IP destination | MAC destination           |     |
|-------|------------|---------------------|-----------------------------|----------------|---------------------------|-----|
| 1     | ping       | `node1` `10.2.1.11` | `node1` `08:00:27:58:80:da` | `8.8.8.8`      |`google``08:00:27:14:94:f7`|     |
| 2     | pong       | `8.8.8.8`           | `google` `08:00:27:14:94:f7`| `10.2.1.11`    |`node1` `08:00:27:58:80:da`| ... |

## III. DHCP

1. Mise en place du serveur DHCP

**🌞Sur la machine node1.net1.tp2, vous installerez et configurerez un serveur DHCP (go Google "rocky linux dhcp server").**

* installation du serveur sur node1.net1.tp2
```
Pour installer le serveur DHCP, il faut faure un : 
dnf -y install dhcp-server 
etc ...
Installed:
  dhcp-server-12:4.3.6-44.el8_4.1.x86_64

Complete!
[maxime@node1 ~]$
```
```
maxime@node1 ~]$ sudo systemctl status dhcpd
● dhcpd.service - DHCPv4 Server Daemon
   Loaded: loaded (/usr/lib/systemd/system/dhcpd.service; enabled; vendor preset: disabled)
   Active: active (running) since Sat 2021-09-25 17:05:46 CEST; 1min 1s ago
     Docs: man:dhcpd(8)
           man:dhcpd.conf(5)
 Main PID: 2499 (dhcpd)
   Status: "Dispatching packets..."
    Tasks: 1 (limit: 11397)
   Memory: 4.9M
   CGroup: /system.slice/dhcpd.service
           └─2499 /usr/sbin/dhcpd -f -cf /etc/dhcp/dhcpd.conf -user dhcpd -group dhcpd --no-pid
```

* créer une machine node2.net1.tp2
* faites lui récupérer une IP en DHCP à l'aide de votre serveur
```
On peut voir que node 2 à récuperé une adresse ip que le DHCP lui a donné:

3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:9c:ca:11 brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.12/24 brd 10.2.1.255 scope global dynamic noprefixroute enp0s8
       valid_lft 842sec preferred_lft 842sec
    inet6 fe80::a00:27ff:fe9c:ca11/64 scope link
       valid_lft forever preferred_lft forever
[maxime@node2 ~]$
```

🌞Améliorer la configuration du DHCP

* ajoutez de la configuration à votre DHCP pour qu'il donne aux clients, en plus de leur IP :

* une route par défaut
* un serveur DNS à utiliser
```

# create new

# specify domain name

option domain-name     "srv.world";
# specify DNS server's hostname or IP address

option domain-name-servers     dlp.srv.world;
# default lease time

default-lease-time 900;
# max lease time

max-lease-time 10800;
# this DHCP server to be declared valid

authoritative;
# specify network address and subnetmask

subnet 10.2.1.0 netmask 255.255.255.0 {
    # specify the range of lease IP address
    range 10.2.1.12 10.2.1.112;
    option routers 10.2.1.254;
    option subnet-mask 255.255.255.0;
    option domain-name-servers 10.2.1.1;
    option domain-name-servers 8.8.8.8;
}
```
* récupérez de nouveau une IP en DHCP sur node2.net1.tp2 pour tester :
* node2.net1.tp2 doit avoir une IP

```
On peut voir que dhclient -r desactive IP mais si on fait un  "sudo dhclient"
le numero de dhcp sur la gauche change donc on peut en conclure que celui-ci à bien recuperer une nouvelle IP
```
**(Voir screen 2)**

* vérifier avec une commande qu'il a récupéré son IP
```
[maxime@node2 ~]$ ip a
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:9c:ca:11 brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.12/24 brd 10.2.1.255 scope global dynamic enp0s8
       valid_lft 888sec preferred_lft 888sec
    inet6 fe80::a00:27ff:fe9c:ca11/64 scope link
       valid_lft forever preferred_lft forever
```
* vérifier qu'il peut ping sa passerelle
```
[maxime@node2 ~]$ ping 10.2.1.254
PING 10.2.1.254 (10.2.1.254) 56(84) bytes of data.
64 bytes from 10.2.1.254: icmp_seq=1 ttl=64 time=0.295 ms
64 bytes from 10.2.1.254: icmp_seq=2 ttl=64 time=0.185 ms
```

* il doit avoir une route par défaut
* vérifier la présence de la route avec une commande
```
[maxime@node2 ~]$ ip n s
10.2.1.254 dev enp0s8 lladdr 08:00:27:14:94:f7 STALE
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:0f REACHABLE
10.2.1.11 dev enp0s8 lladdr 08:00:27:58:80:da STALE
10.0.2.2 dev enp0s3 lladdr 52:54:00:12:35:02 STALE
[maxime@node2 ~]$
```
vérifier que la route fonctionne avec un ping vers une IP
```
[maxime@node2 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=112 time=19.6 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=112 time=18.7 ms
```

il doit connaître l'adresse d'un serveur DNS pour avoir de la résolution de noms
```
J'ai mis dans la config dhcp , le dns de google 
[maxime@node2 ~]$ ping github.com
PING github.com (140.82.121.4) 56(84) bytes of data.
64 bytes from lb-140-82-121-4-fra.github.com (140.82.121.4): icmp_seq=1 ttl=52 time=28.6 ms
64 bytes from lb-140-82-121-4-fra.github.com (140.82.121.4): icmp_seq=2 ttl=52 time=28.4 ms
```

* vérifier avec la commande dig que ça fonctionne
* vérifier un ping vers un nom de domaine
```
[maxime@node2 ~]$ dig github.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> github.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 37692
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 512
;; QUESTION SECTION:
;github.com.                    IN      A

;; ANSWER SECTION:
github.com.             60      IN      A       140.82.121.3

;; Query time: 28 msec
;; SERVER: 8.8.8.8#53(8.8.8.8)
;; WHEN: Sat Sep 25 18:04:29 CEST 2021
;; MSG SIZE  rcvd: 55

[maxime@node2 ~]$
```

## 2. Analyse de trames

**🌞Analyse de trames**

* videz les tables ARP des machines concernées
```
voici la commande : sudo ip neigh flush all
```
* lancer une capture à l'aide de tcpdump afin de capturer un échange DHCP
`sudo tcpdump -i enp0s8 -c 50 -w tp2_arp.pcap not port 22`
* choisissez vous-mêmes l'interface où lancer la capture
`je fais la capture sur enp0s8`

* répéter une opération de renouvellement de bail DHCP, et demander une nouvelle IP afin de générer un échange DHCP
* exportez le fichier .pcap

* mettez en évidence l'échange DHCP DORA (Disover, Offer, Request, Acknowledge)
écrivez, dans l'ordre, les échanges ARP + DHCP qui ont lieu, je veux TOUTES les trames utiles pour l'échange

| ordre | type trame | IP source           | MAC source                  | IP destination  | MAC destination           |     |
|-------|------------|---------------------|-----------------------------|-----------------|---------------------------|-----|
| 1     | DHCP       |  0.0.0.0            | `node2` `08:00:27:9c:ca:11` |`255.255.255.255`|`google``08:00:27:14:94:f7`|     |
| 2     | DHCP       | `10.2.1.11`         | `node1` `08:00:27:58:80:da` | `10.2.1.12`     |`node2` `08:00:27:9c:ca:11`| ... |
| 3     | DHCP       |  0.0.0.0            | `node2` `08:00:27:9c:ca:11` |`255.255.255.255`|`google``08:00:27:14:94:f7`|     |
| 4     | DHCP       | `10.2.1.11`         | `node1` `08:00:27:58:80:da` | `10.2.1.12`     |`node2` `08:00:27:9c:ca:11`| ... |
| 5     | Rêquete ARP| `10.2.1.12`         | `node2` `08:00:27:9c:ca:11` | `10.2.1.12`     |`node2``ff:ff:ff:ff:ff:ff` |     |
| 6     | Rêquete ARP| `10.2.1.11`         | `node1` `08:00:27:58:80:da` | `10.2.1.12`     |`node2` `08:00:27:9c:ca:11`| ... |
| 7     | Reponse ARP| `10.2.1.12`         | `node1` `08:00:27:58:80:da` | `10.2.1.11`     |`node2` `08:00:27:9c:ca:11`|     |
| 8     | Reponse ARP| `10.2.1.12`         | `node2` `08:00:27:9c:ca:11` | `10.2.1.11`     |`node1` `08:00:27:58:80:da`| ... |