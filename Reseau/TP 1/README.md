# **TP1_B2 - Mise en jambes**
**1. Affichez les infos des cartes réseau de votre PC**

🌞 nom, adresse MAC et adresse IP de l'interface WiFi
``` 
Pour afficher ces informations il faut faire un "ipconfig /all "
```
```
Carte réseau sans fil Wi-Fi :

Description. . . . . . . . . . . . . . : Intel(R) Wi-Fi 6 AX201 160MHz
Adresse physique . . . . . . . . . . . : F8-AC-65-DA-00-C5
Adresse IPv4. . . . . . . . . . . . . .: 10.33.3.48(préféré)
etc...
```
-----
🌞 nom, adresse MAC et adresse IP de l'interface Ethernet
``` 
Pour afficher ces informations il faut faire un "ipconfig /all "
```
```
PS : Pas de port ethernet 
```
**2. Affichez votre gateway**
🌞 utilisez une commande pour connaître l'adresse IP de la passerelle de votre carte WiFi

``` 
Pour afficher ces informations il faut faire un "ipconfig /all "
```
```
Carte réseau sans fil Wi-Fi :
   Passerelle par défaut. . . . . . . . . : 10.33.3.253
   etc ...
```

**3.Trouvez comment afficher les informations sur une carte IP (change selon l'OS)**

🌞 trouvez l'IP, la MAC et la gateway pour l'interface WiFi de votre PC

```
Pour pouvoir trouver ces informations avec la GUI , il faut cliquer sur l'icône de connexion réseau, en bas à droite de la barre des tâches, puis faites un clic droit puis sélectionnez « Ouvrir les paramètres réseau et Internet ».

Dans la fenêtre de l'état du réseau qui s'affiche, il faut cliquer sur le lien « Afficher vos propriétés réseau ». Et on obtient 
```
- **(Voir Screen 1)**

**4.Questions**

🌞à quoi sert la gateway dans le réseau d'YNOV ?
```
La gateway permet de relier deux réseaux informatiques de types différents, c'est un dire de permettre au reseau local de ce connecter à internet 
```

**5. Modifications des informations**

🌞- Utilisez l'interface graphique de votre OS pour changer d'adresse IP : 
🌞 ne changez que le dernier octet
    par exemple pour 10.33.1.10, echangez que le "10"
    valeur entre 1 et 254 compris

```
Pour pouvoir trouver ces informations avec la GUI , il faut cliquer sur l'icône de connexion réseau, en bas à droite de la barre des tâches, puis faites un clic droit puis sélectionnez « Ouvrir les paramètres réseau et Internet ».

Dans la fenêtre de l'état du réseau qui s'affiche, il faut cliquer sur "Propriete" (sur le reseau connecté).Sur cette nouvelle page, il faudra cliquer sur modifier , ensuite sur manel et il faut choisir IPv4 . 

Une fois cela fait, on peut changer l'adresse IP de votre carte WiFi pour une autre.
```

- **(Voir Screen 2)**

- Il est possible que vous perdiez l'accès internet. Que ce soit le cas ou non, expliquez pourquoi c'est possible de perdre son accès internet en faisant cette opération.

```
Vu que le serveur DHCP de Ynov donne une adresse IP à mon pc , si je lui rentre une nouvelle adresse IP, je ne ferais plus parti du réseau et donc je perdrais le Wifi 
```

**6.B. Table ARP**

**🌞 Exploration de la table ARP**
* depuis la ligne de commande, afficher la table ARP
```
J'ai utilisé la commande "arp -a " pour pouvoir afficher la table
```
* identifier l'adresse MAC de la passerelle de votre réseau, et expliquer comment vous avez repéré cette adresse MAC spécifiquement

```
Interface : 10.33.3.48 --- 0x3
  Adresse Internet      Adresse physique      Type
  10.33.3.253           00-12-00-40-4c-bf     dynamique
```
```
J'ai regardé l'adresse ip que j'avais sur ma carte wifi et j'ai cherché dans la table arp sa liste. 
Une fois que je l'ai trouvé , j'ai remarqué que la ligne de l'adresse 10.33.3.253 etait en dynamique (ce qui correspond au reseau Ynov)
Mais aussi si je regarde l'adresse cité juste avant je remarque que cette adresse est ma gateway.
```

🌞 Et si on remplissait un peu la table ?
* envoyez des ping vers des IP du même réseau que vous. Lesquelles ? menfou, random. Envoyez des ping vers au moins 3-4 machines.
* affichez votre table ARP
* listez les adresses MAC associées aux adresses IP que vous avez ping
```
C:\Users\Max>ping 10.33.3.130

Envoi d’une requête 'Ping'  10.33.3.130 avec 32 octets de données :
Réponse de 10.33.3.130 : octets=32 temps=9 ms TTL=128
Réponse de 10.33.3.130 : octets=32 temps=3 ms TTL=128
Réponse de 10.33.3.130 : octets=32 temps=4 ms TTL=128

C:\Users\Max>arp -a

Interface : 10.33.3.48 --- 0x3
  Adresse Internet      Adresse physique      Type
  10.33.2.97            d8-12-65-b5-11-77     dynamique
  10.33.3.17            80-32-53-e2-78-04     dynamique
  10.33.3.109           38-f9-d3-2f-c2-79     dynamique
  10.33.3.112           3c-06-30-2d-48-0d     dynamique
  10.33.3.130           18-1d-ea-b4-8c-82     dynamique
  10.33.3.253           00-12-00-40-4c-bf     dynamique
  10.33.3.254           00-0e-c4-cd-74-f5     dynamique
  10.33.3.255           ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  224.0.1.60            01-00-5e-00-01-3c     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
  
```
```
Les adresses MAC sont :  
18-1d-ea-b4-8c-82 =  10.33.3.130
3c-06-30-2d-48-0d =  10.33.3.112 
38-f9-d3-2f-c2-79 =  10.33.3.109
```

🌞 Utilisez nmap pour scanner le réseau de votre carte WiFi et trouver une adresse IP libre
```
Une fois la scan effectué , grace au filtre j'ai pu trouver une adresse libre "10.33.3.50"
```
```
Starting Nmap 7.92 ( https://nmap.org ) at 2021-09-13 17:02 Paris, Madrid (heure d’été)

Nmap scan report for 10.33.0.3

Host is up (0.0070s latency).

MAC Address: 22:93:D2:7E:9F:1A (Unknown)

Nmap scan report for 10.33.0.10

Host is up (0.42s latency).

MAC Address: B0:6F:E0:4C:CF:EA (Samsung Electronics)

Nmap scan report for 10.33.0.20

Host is up (0.0070s latency).

MAC Address: 74:29:AF:33:1B:69 (Hon Hai Precision Ind.)

Nmap scan report for 10.33.0.21

Host is up (0.0060s latency).

MAC Address: B0:FC:36:CE:9C:89 (CyberTAN Technology)

Nmap scan report for 10.33.0.27

Host is up (0.0040s latency).

MAC Address: A8:64:F1:8B:1D:4D (Intel Corporate)

Nmap scan report for 10.33.0.36

Host is up (0.11s latency).

MAC Address: 96:96:02:CE:54:2F (Unknown)

Nmap scan report for 10.33.0.41

Host is up (0.36s latency).
etc ...
```
* Réinitialiser votre conf réseau (reprenez une IP automatique, en vous déconnectant/reconnectant au réseau par exemple)
```
Je me suis deco/reco et j'ai reçu cette nouvelle adresse  Adresse IPv4. . . . . . . . . . . . . .: 10.33.3.48
```
* Lancez un scan de ping sur le réseau YNOV
```
Starting Nmap 7.92 ( https://nmap.org ) at 2021-09-16 14:50 Paris, Madrid (heure d’été)

Nmap scan report for 10.33.0.10

Host is up (0.46s latency).

MAC Address: B0:6F:E0:4C:CF:EA (Samsung Electronics)

Nmap scan report for 10.33.0.13

Host is up (0.66s latency).

MAC Address: D2:50:0E:26:94:8B (Unknown)

Nmap scan report for 10.33.0.21

Host is up (0.015s latency).

MAC Address: B0:FC:36:CE:9C:89 (CyberTAN Technology)

Nmap scan report for 10.33.0.27

Host is up (0.0050s latency).

MAC Address: A8:64:F1:8B:1D:4D (Intel Corporate)

Nmap scan report for 10.33.0.31

Host is up (0.17s latency).

MAC Address: FA:C5:6D:60:4B:4C (Unknown)

Nmap scan report for 10.33.0.38

Host is up (0.0030s latency).

MAC Address: 84:1B:77:F9:21:71 (Intel Corporate)

Nmap scan report for 10.33.0.54

Host is up (0.013s latency).

MAC Address: D0:3C:1F:FD:6F:88 (Intel Corporate)

Nmap scan report for 10.33.0.58

etc...
```
* affichez votre table ARP
```
Voici ma table ARP: 

Interface : 10.33.3.48 --- 0x3
  Adresse Internet      Adresse physique      Type
  10.33.0.3             22-93-d2-7e-9f-1a     dynamique
  10.33.0.24            d8-12-65-b5-11-77     dynamique
  10.33.0.27            a8-64-f1-8b-1d-4d     dynamique
  10.33.0.208           70-66-55-c5-4e-29     dynamique
  10.33.0.210           70-66-55-47-54-15     dynamique
  10.33.0.244           ce-3d-70-5e-c8-db     dynamique
  10.33.1.8             0c-dd-24-aa-e1-87     dynamique
  10.33.1.118           a2-24-4b-f2-bf-6a     dynamique
  10.33.1.138           a2-89-f5-eb-77-0b     dynamique
  10.33.1.232           30-35-ad-af-17-42     dynamique
  10.33.2.10            84-fd-d1-10-23-45     dynamique
  10.33.2.14            84-fd-d1-10-23-45     dynamique
  10.33.2.64            90-9c-4a-ba-53-f6     dynamique
  10.33.2.101           18-56-80-cc-ea-3e     dynamique
  10.33.2.149           82-2d-4a-c7-24-c4     dynamique
  10.33.2.188           d8-f3-bc-c2-f5-39     dynamique
  10.33.2.209           5c-87-9c-e4-44-2c     dynamique
  10.33.2.237           be-fd-16-82-f4-05     dynamique
  10.33.3.23            7e-7e-4e-bb-23-88     dynamique
  10.33.3.24            ac-12-03-2e-e4-92     dynamique
  10.33.3.41            3c-58-c2-14-aa-5a     dynamique
  10.33.3.65            48-e7-da-69-32-77     dynamique
  10.33.3.108           80-32-53-e2-78-04     dynamique
  10.33.3.170           80-32-53-e2-78-04     dynamique
  10.33.3.187           96-fd-87-13-4b-ee     dynamique
  10.33.3.189           c2-6f-43-3d-c7-fa     dynamique
  10.33.3.207           c0-e4-34-1b-fe-a9     dynamique
  10.33.3.253           00-12-00-40-4c-bf     dynamique
  10.33.3.255           ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
  ```
 **D. Modification d'adresse IP (part 2)**
  
* 🌞 Modifiez de nouveau votre adresse IP vers une adresse IP que vous savez libre grâce à nmap
```
J'ai décider d'utiliser IP = 10.33.0.50 qui est libre 
```

* utilisez un ping scan sur le réseau YNOV
 montrez moi la commande nmap et son résultat
 ```
 Voici la commande : nmap -sn 10.33.0.0/22 -n
 Et le resultat : 
 PS C:\Program Files (x86)\Nmap> nmap -sP 10.33.0.0/22 -n
Starting Nmap 7.92 ( https://nmap.org ) at 2021-09-16 15:11 Paris, Madrid (heure dÆÚtÚ)
Stats: 0:00:00 elapsed; 0 hosts completed (0 up), 1023 undergoing ARP Ping Scan
ARP Ping Scan Timing: About 1.37% done; ETC: 15:11 (0:00:00 remaining)
etc ... 
MAC Address: 00:1E:4F:F9:BE:14 (Dell)
Nmap scan report for 10.33.3.253
Host is up (0.064s latency).
MAC Address: 00:12:00:40:4C:BF (Cisco Systems)
Nmap scan report for 10.33.3.254
Host is up (0.062s latency).
MAC Address: 00:0E:C4:CD:74:F5 (Iskra Transmission d.d.)
Nmap scan report for 10.33.0.50
Host is up.
Nmap done: 1024 IP addresses (150 hosts up) scanned in 28.96 seconds
```
 * configurez correctement votre gateway pour avoir accès à d'autres réseaux (utilisez toujours la gateway d'YNOV)
```
La gateway est 10.33.3.254
```
* prouvez en une suite de commande que vous avez bien l'IP choisie, que votre passerelle est bien définie, et que vous avez un accès internet
```
Carte réseau sans fil Wi-Fi :

Je fais un "ipconfig /all"
Adresse IPv4. . . . . . . . . . . .  .: 10.33.0.50(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Passerelle par défaut. . . . . . . . . : 10.33.3.253

Et pour prouver qu'on est bien connecté à internet , il faut ping sur cette adresse : 

C:\Users\Max>ping 1.1.1.1

Envoi d’une requête 'Ping'  1.1.1.1 avec 32 octets de données :
Réponse de 1.1.1.1 : octets=32 temps=18 ms TTL=58
Réponse de 1.1.1.1 : octets=32 temps=17 ms TTL=58
Réponse de 1.1.1.1 : octets=32 temps=18 ms TTL=58
```

🌞Si vos PCs ont un port RJ45 alors y'a une carte réseau Ethernet associée :

* modifiez l'IP des deux machines pour qu'elles soient dans le même réseau

* choisissez une IP qui commence par "192.168"
utilisez un /30 (que deux IP disponibles)
```
Nous avons choisis les deux IP dispo = 192.168.1.1 et 192.168.1.2 
```

* vérifiez à l'aide de commandes que vos changements ont pris effet
```
On peut le verifier grace a "ipconfig /all"
Carte Ethernet Ethernet :

Suffixe DNS propre à la connexion. . . :
Adresse IPv6 de liaison locale. . . . .: fe80::e064:f57e:b254:6142%8
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.1.2
   Masque de sous-réseau. . . . . . . . . : 255.255.255.252
   Passerelle par défaut. . . . . . . . . : 192.168.1.3
```
* utilisez ping pour tester la connectivité entre les deux machines
```
C:\Users\Max>ping 192.168.1.1

Envoi d’une requête 'Ping'  192.168.1.1 avec 32 octets de données :
Réponse de 192.168.1.1 : octets=32 temps=1 ms TTL=64
Réponse de 192.168.1.1 : octets=32 temps=1 ms TTL=64
```
* affichez et consultez votre table ARP
```
Grace à la commande "arp -a"
Interface : 192.168.1.2 --- 0x8
  Adresse Internet      Adresse physique      Type
  192.168.1.1           98-28-a6-0d-a3-5d     dynamique
  192.168.1.3           ff-ff-ff-ff-ff-ff     statique
  etc...
 ```
 **4. Utilisation d'un des deux comme gateway**

🌞 pour tester la connectivité à internet on fait souvent des requêtes simples vers un serveur internet connu

encore une fois, un ping vers un DNS connu comme 1.1.1.1 ou 8.8.8.8 c'est parfait
```
C:\Users\Max>ping 1.1.1.1

Envoi d’une requête 'Ping'  1.1.1.1 avec 32 octets de données :
Réponse de 1.1.1.1 : octets=32 temps=159 ms TTL=114

C:\Users\Max>ping 8.8.8.8

Envoi d’une requête 'Ping'  8.8.8.8 avec 32 octets de données :
Réponse de 8.8.8.8 : octets=32 temps=159 ms TTL=114
```
🌞 utiliser un traceroute ou tracert pour bien voir que les requêtes passent par la passerelle choisie (l'autre le PC)
```
C:\Users\Max>tracert -j 192.168.1.1

Détermination de l’itinéraire vers LAPTOP-CANRQJ6J [192.168.1.1]
avec un maximum de 30 sauts :

  1  LAPTOP-CANRQJ6J [192.168.1.1]  rapports : Itinéraire source spécifié non valide.

Itinéraire déterminé.
```
🌞 sur le PC client avec par exemple l'IP 192.168.1.2
*  nc.exe 192.168.1.1 8888
*  "netcat, connecte toi au port 8888 de la machine 192.168.1.1 stp"
* une fois fait, vous pouvez taper des messages dans les deux sens
```
C:\Users\Max\Downloads\netcat-1.11>.\nc.exe 192.168.1.1 8888
bBonjour (pc client) 
sRIOMnhwdrtùh (pc client)
TP RESEAU(pc serveur)
TEST(pc serveur)
```
🌞 pour aller un peu plus loin

* le serveur peut préciser sur quelle IP écouter, et ne pas répondre sur les autres
par exemple, on écoute sur l'interface Ethernet, mais pas sur la WiFI
pour ce faire nc.exe -l -p PORT_NUMBER IP_ADDRESS

* par exemple nc.exe -l -p 9999 192.168.1.37

* on peut aussi accepter uniquement les connexions internes à la machine en écoutant sur 127.0.0.1
```
C:\Users\Max\Downloads\netcat-1.11>nc.exe  192.168.1.1 8888
RE(pc client)
OK (pc serveur)
ok (pc client)
```
🌞 Autoriser les ping

* configurer le firewall de votre OS pour accepter le ping
* aidez vous d'internet
on rentrera dans l'explication dans un prochain cours mais sachez que ping envoie un message ICMP de type 8 (demande d'ECHO) et reçoit un message ICMP de type 0 (réponse d'écho) en retour

```
Il faut faire :
PS C:\WINDOWS\system32> netsh advfirewall firewall add rule name="ICMP Allow incoming V4 echo request" protocol=icmpv4:8,any dir=in action=allow
Ok.

PS C:\WINDOWS\system32> ping 192.168.1.1

Envoi d’une requête 'Ping'  192.168.1.1 avec 32 octets de données :
Réponse de 192.168.1.1 : octets=32 temps=1 ms TTL=64
Réponse de 192.168.1.1 : octets=32 temps=1 ms TTL=64

On peut voir, que je peux ping avec le firewall d'activé
```
🌞 Autoriser le traffic sur le port qu'utilise nc
```
C:\Users\Max\Downloads\netcat-1.11>nc.exe  192.168.1.1 1025
port 1025 OK (pc client)
OK (pc serveur)
```
**III. Manipulations d'autres outils/protocoles côté client**

**1. DHCP**

🌞Exploration du DHCP, depuis votre PC
```
Carte réseau sans fil Wi-Fi :
   Bail obtenu. . . . . . . . . . . . . . : jeudi 16 septembre 2021 16:37:28
   Bail expirant. . . . . . . . . . . . . : jeudi 23 septembre 2021 16:41:39
   Serveur DHCP . . . . . . . . . . . . . : 192.168.137.1
   
fonctionnement de DHCP : le DHCP fonctionne sur le modèle client-serveur : un serveur, qui détient la politique d'attribution des configurations IP, envoie une configuration donnée pour une durée donnée à un client donné (typiquement, une machine qui vient de démarrer).
```

**2. DNS**

🌞 trouver l'adresse IP du serveur DNS que connaît votre ordinateur
```
Carte réseau sans fil Wi-Fi : 

   Serveurs DNS. . .  . . . . . . . . . . : 192.168.137.1
```
🌞 utiliser, en ligne de commande l'outil nslookup (Windows, MacOS) ou dig (GNU/Linux, MacOS) pour faire des requêtes DNS à la main

faites un lookup (lookup = "dis moi à quelle IP se trouve tel nom de domaine")

pour google.com
```
PS C:\WINDOWS\system32> nslookup google.com
Serveur :   LAPTOP-CANRQJ6J.mshome.net
Address:  192.168.137.1

Réponse ne faisant pas autorité :
Nom :    google.com
Addresses:  2a00:1450:400e:80d::200e
          216.58.206.238
```
pour ynov.com
```
PS C:\WINDOWS\system32> nslookup ynov.com
Serveur :   LAPTOP-CANRQJ6J.mshome.net
Address:  192.168.137.1

Réponse ne faisant pas autorité :
Nom :    ynov.com
Address:  92.243.16.143
```
interpréter les résultats de ces commandes
```
On remarque que mon serveur dns est bien LAPTOP-CANRQJ6J.mshome.net
````
* déterminer l'adresse IP du serveur à qui vous venez d'effectuer ces requêtes
faites un reverse lookup (= "dis moi si tu connais un nom de domaine pour telle IP")

* pour l'adresse 78.74.21.21
```
PS C:\WINDOWS\system32> nslookup 78.74.21.21
Serveur :   LAPTOP-CANRQJ6J.mshome.net
Address:  192.168.137.1

Nom :    host-78-74-21-21.homerun.telia.com
Address:  78.74.21.21
```

* pour l'adresse 92.146.54.88
```
PS C:\WINDOWS\system32> nslookup 92.146.54.88
Serveur :   LAPTOP-CANRQJ6J.mshome.net
Address:  192.168.137.1

Nom :    apoitiers-654-1-167-88.w92-146.abo.wanadoo.fr
Address:  92.146.54.88
```
* interpréter les résultats:
si vous vous demandez, j'ai pris des adresses random :)
```
Grace au reverse lookup, on remarque que j'ai toujours le meme serveur dns et que l'adresse du site correpond à l'IP
```
**IV. Wireshark**

🌞 utilisez le pour observer les trames qui circulent entre vos deux carte Ethernet. Mettez en évidence :

* un ping entre vous et la passerelle
**(screen 3)**
* un netcat entre vous et votre mate, branché en RJ45
**(screen 4)**
* une requête DNS. Identifiez dans la capture le serveur DNS à qui vous posez la question.
**(screen 5)**
