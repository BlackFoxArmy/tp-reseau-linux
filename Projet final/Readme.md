# Projet final ( server VPN)
Maxime Chasson ,Gregoire Mathieu
# Introduction 

* Pour ce TP, nous voulons mettre en place un serveur VPN qui donnerais aux clients acces au serveur WEB. Pour cela nous utiliserons gns3 pour pouvoir utiliser des routeurs ou switchs si besoin. 

# Sommaire
- [Projet final ( server VPN)](#projet-final--server-vpn)
- [Introduction](#introduction)
- [Sommaire](#sommaire)
- [I. Prérequis](#i-prérequis)
  - [📝 Checklist VM Linux 📝](#-checklist-vm-linux-)
- [1. Topologie 1](#1-topologie-1)
  - [2. Adressage topologie 1](#2-adressage-topologie-1)
  - [3. Setup topologie 1](#3-setup-topologie-1)
- [II. VLAN](#ii-vlan)
  - [1. Setup Topologie 2](#1-setup-topologie-2)
    - [Adressage](#adressage)
- [III Routeur](#iii-routeur)
  - [Topologie 3](#topologie-3)
- [Switch 3 et VPN](#switch-3-et-vpn)
  - [Topologie 4](#topologie-4)
    - [1.Setup du troisieme Switch](#1setup-du-troisieme-switch)
    - [Mise en place du cloud](#mise-en-place-du-cloud)
- [Configurer OpenVPN sur Rocky Linux 8](#configurer-openvpn-sur-rocky-linux-8)
  - [Installation de EPEL Repository](#installation-de-epel-repository)
  - [Installation de OpenVPN](#installation-de-openvpn)
  - [Installation de Easy-RSA](#installation-de-easy-rsa)
  - [Creation de la cle publique OpenVPN](#creation-de-la-cle-publique-openvpn)
  - [Création des cles et du certificat pour le serveur vpn](#création-des-cles-et-du-certificat-pour-le-serveur-vpn)
  - [Création de la cle HMAC](#création-de-la-cle-hmac)
  - [Création certificat de revocation](#création-certificat-de-revocation)
  - [Création du certificat et de la clé client](#création-du-certificat-et-de-la-clé-client)
  - [Copie de la cle et du certificat client dans le dossier client](#copie-de-la-cle-et-du-certificat-client-dans-le-dossier-client)
  - [Configuration du vpn](#configuration-du-vpn)
  - [Configuration des routes du vpn](#configuration-des-routes-du-vpn)
- [Installer et configurer un client pour le vpn](#installer-et-configurer-un-client-pour-le-vpn)
  - [Installation de Epel-Release et OpenVPN](#installation-de-epel-release-et-openvpn)
  - [Configuration du client](#configuration-du-client)
  - [Se connecter au serveur vpn](#se-connecter-au-serveur-vpn)
- [NFS : Configuration du serveur NFS](#nfs--configuration-du-serveur-nfs)
  - [Configuration du serveur NFS](#configuration-du-serveur-nfs)
  - [Autoriser le service NFS](#autoriser-le-service-nfs)
- [Installer le serveur Apache (web)](#installer-le-serveur-apache-web)
      - [Démarrer le service Apache](#démarrer-le-service-apache)
- [Les Moindres privileges](#les-moindres-privileges)
- [Conclusion](#conclusion)


# I. Prérequis

* Voici la topologie final de ce tp sur le serveur VPN
![Topologie 0](./screen/schema.jpg)
* Pour commencer, il nous faudra : 

    ➜  5 VMs Rocky Linux

    ➜ Les switches Cisco des vIOL2 (3)

    ➜ Les routeurs Cisco des c3640 (1)

➜ **Vous ne créerez aucune machine virtuelle au début. Vous les créerez au fur et à mesure que le TP vous le demande.** A chaque fois qu'une nouvelle machine devra être créée, vous trouverez l'emoji 🖥️ avec son nom.

## 📝 Checklist VM Linux 📝

Pour que vos VMs soit bien configuré pour la suite du tp, il faudra remplir ces conditions si-dessous .

- [x] IP locale, statique ou dynamique
- [x] hostname défini
- [x] firewall actif, qui ne laisse passer que le strict nécessaire
- [x] on force une host-only, juste pour pouvoir SSH
- [x] SSH fonctionnel
- [x] résolution de nom
  - vers internet, quand vous aurez le routeur en place

**Les éléments de la 📝checklist📝 sont STRICTEMENT OBLIGATOIRES à réaliser mais ne doivent PAS figurer dans le rendu.**


Les réseaux et leurs VLANs associés :

| Réseau    | Adresse       | VLAN associé |
| --------- | ------------- | ------------ |
| `clients` | `10.1.1.0/24` | 10           |
| `admins`  | `10.2.2.0/24` | 20           |
| `servers` | `10.3.3.0/24` | 30           |

L'adresse des machines au sein de ces réseaux :

| Node               | `clients`       | `admins`        | `servers`       |
| ------------------ | --------------- | --------------- | --------------- |
| `pc1.clients.tp3`  | `10.1.1.1/24`   | x               | x               |
| `pc2.clients.tp3`  | `10.1.1.2/24`   | x               | x               |
| `vpn.servers.tp3`  | x               | `10.2.2.2/24`   | x               |
| `nfs.servers.tp3`  | x               | `10.2.2.3/24`   | x               |
| `web1.servers.tp3` | x               | x               | `10.3.3.1/24`   |
| `r1`               | `10.1.1.254/24` | `10.2.2.254/24` | `10.3.3.254/24` |

# 1. Topologie 1

![Topologie 1](./screen/topo1.jpg)


test

## 2. Adressage topologie 1
🖥️  `pc1.clients.tp3` et `pc2.clients.tp3`

* N'oublier pas la 📝 Checklist VM Linux 📝pour la config de ces VMs 
* N'oublier pas d'importer les VMs sur GNS3 (si celui-ci est utilisé réalisé ce tp )

| Node              | IP            |
| ----------------- | ------------- |
| `pc1.clients.tp3` | `10.1.1.1/24` |
| `pc2.clients.tp3` | `10.1.1.2/24` |

## 3. Setup topologie 1

 **Commençons simple**

- définissez les IPs statiques sur les deux VMs " 

> Peu importe les adresse tant qu'elle corresponde à votre réseau que vous avez défini auparavant 

- On vérifie que PC1 peut  `ping` PC2 

> Petit précision si il  n'y a pas de conf sur le switch. Tant qu'on ne fait rien, c'est une bête multiprise.

```
[maxime@pc1 ~]$ ping 10.1.1.2
PING 10.1.1.2 (10.1.1.2) 56(84) bytes of data.
64 bytes from 10.1.1.2: icmp_seq=1 ttl=64 time=4.36 ms
64 bytes from 10.1.1.2: icmp_seq=2 ttl=64 time=5.39 ms

[maxime@pc2 ~]$ ping 10.1.1.1
PING 10.1.1.1 (10.1.1.1) 56(84) bytes of data.
64 bytes from 10.1.1.1: icmp_seq=1 ttl=64 time=18.8 ms
64 bytes from 10.1.1.1: icmp_seq=2 ttl=64 time=8.15 ms
```
# II. VLAN

## 1. Setup Topologie 2

### Adressage

Les réseaux et leurs VLANs associés :

| Réseau    | Adresse       | VLAN associé |
| --------- | ------------- | ------------ |
| `clients` | `10.1.1.0/24` | 10           |
| `admins`  | `10.2.2.0/24` | 20           |
| `servers` | `10.3.3.0/24` | 30           |


Nous connaissons deja les deux anciennes machines (pc1 et pc2)

**Configuration des VLANs**


- Pour déclarer des VLANs sur le switch `sw1`: 
```
➜ 1. Passer en mode configuration

# conf t

➜ 2. Définir les VLANs à utiliser
(config)# vlan 10
(config-vlan)# name clients
(config-vlan)# exit

(config)# vlan 20
(config-vlan)# name admins
(config-vlan)# exit

(config)# vlan 30
(config-vlan)# name servers
(config-vlan)# exit
(config)# exit
```
```
# show vlan
VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
1    default                          active    Gi0/0, Gi0/1, Gi0/2, Gi0/3
                                                Gi1/0, Gi1/1, Gi1/2, Gi1/3
                                                Gi2/0, Gi2/1, Gi2/2, Gi2/3
                                                Gi3/0, Gi3/1, Gi3/2, Gi3/3
10   clients                          active
20   admins                           active
30   servers                          active
1002 fddi-default                     act/unsup
1003 token-ring-default               act/unsup

➜ 3. Attribuer à chaque interface du switch un VLAN

# conf t
Switch(config)#interface gigabitEthernet0/0
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 10
Switch(config-if)#exit

Switch(config)#interface gigabitEthernet0/1
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 10
Switch(config-if)#exit
Switch(config)#

```
- ici, tous les ports sont en mode access : ils pointent vers des clients du réseau
  
- il faudra ajouter le port qui pointe vers le *routeur* comme un *trunk* : c'est un port entre deux équipements réseau (un *switch* et un *routeur*)

```
Switch(config)#interface gigabitEthernet1/0
Switch(config-if)#switchport trunk encapsulation dot1q
Switch(config-if)#switchport mode trunk
Switch(config-if)#switchport trunk allowed vlan add 10,20,30
Switch(config-if)#exit
Switch#show interfaces trunk

Switch#sh interfaces trunk

Port        Mode             Encapsulation  Status        Native vlan
Gi1/0       on               802.1q         trunking      1

Port        Vlans allowed on trunk
Gi1/0       1-4094

Port        Vlans allowed and active in management domain
Gi1/0       1,10,20,30

Port        Vlans in spanning tree forwarding state and not pruned
Gi1/0       1,10,20,30

```
# III Routeur

## Topologie 3
![Topologie 3](./screen/topo2.jpg)

- Mettre en place un deuxième switch et un routeur
> Ne pas oublier de configurer le deuxième switch en créant un trunk sur l'interface d'entrée du switch 1 et un autre trunk vers l'interface de sortie du routeur 
  
**Config du *routeur***

- attribuez ses IPs au *routeur*
  - 3 sous-interfaces, chacune avec son IP et un VLAN associé

```
Avec un "show running-config" on peut voir la config sur complete du routeur . On remarque que nos sous interfaces ont bien etait crées .

VLAN 10 = 

R1(config)#interface fastEthernet 0/0.10
R1(config-subif)#encapsulation dot1Q 10
R1(config-subif)#ip addr 10.1.1.254 255.255.255.0
R1(config-subif)#exit

VLAN 20 =

R1(config)#interface fastEthernet 0/0.20
R1(config-subif)#encapsulation dot1Q 20
R1(config-subif)#ip addr 10.2.2.254 255.255.255.0
R1(config-subif)#exit

VLAN 30 =
R1(config)#interface fastEthernet 0/0.30
R1(config-subif)#encapsulation dot1Q 30
R1(config-subif)#ip addr 10.3.3.254 255.255.255.0
R1(config-subif)#exit
```

* Il ne faudra pas oublier de faire du routage intervlan pour que le vpn puisse communiqué avec les deux machines clients 
# Switch 3 et VPN

## Topologie 4

![Topologie 4](./screen/topo3.jpg)

### 1.Setup du troisieme Switch

* Pour celui-ci, on reste sur la même configuration que les anciens.C'est à dire configurer nos vlans pour les reseaux (admins, servers et clients)(10, 20, 30)
```
(config)# vlan 10
(config-vlan)# name clients
(config-vlan)# exit

(config)# vlan 20
(config-vlan)# name admins
(config-vlan)# exit

(config)# vlan 30
(config-vlan)# name servers
(config-vlan)# exit
(config)# exit
```
* Nous allons mettre en place un blocage de trafic sur le firewall , se dernier depend de ce que vous voulez autoriser comme trafic que ce soit du nfs,dns,ssh etc...

Voici quelque exemple de commande :

```
# Interdire toute connexion entrante et sortante 
iptables -P INPUT DROP 
iptables -P FORWARD DROP 
iptables -P OUTPUT DROP 
 
# Autoriser RELATED et ESTABLISHED
# Tuer INVALID 
iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -A INPUT -m state --state INVALID -j DROP
iptables -A OUTPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -A OUTPUT -m state --state INVALID -j DROP
 
# Laisser certains types ICMP
iptables -A INPUT -p icmp --icmp-type destination-unreachable -j ACCEPT
iptables -A INPUT -p icmp --icmp-type source-quench -j ACCEPT
iptables -A INPUT -p icmp --icmp-type time-exceeded -j ACCEPT
iptables -A INPUT -p icmp --icmp-type parameter-problem -j ACCEPT
iptables -A INPUT -p icmp --icmp-type echo-request -j ACCEPT
 
# SSH In 
iptables -A INPUT -p tcp --dport 2222 -j ACCEPT 
 
# DNS In/Out 
iptables -A OUTPUT -p udp --dport 53 -j ACCEPT
iptables -A OUTPUT -p tcp --dport 53 -j ACCEPT
iptables -A INPUT -p udp --dport 53 -j ACCEPT 
iptables -A INPUT -p tcp --dport 53 -j ACCEPT
 
# NTP Out 
iptables -A OUTPUT -p udp --dport 123 -j ACCEPT 
 
# NTP In
iptables -A INPUT -p udp --dport 123 -j ACCEPT
 
# HTTP + HTTPS In & Out
iptables -A INPUT -p tcp --dport 80 -j ACCEPT 
iptables -A INPUT -p tcp --dport 443 -j ACCEPT 
iptables -A INPUT -p tcp --dport 8443 -j ACCEPT 
 
# Remote DD-WRT
iptables -A INPUT -p tcp --dport 9999 -j ACCEPT 
iptables -A OUTPUT -p tcp --dport 9999 -j ACCEPT
 
 
# Mail SMTP:25 bidirectionnel
iptables -A INPUT -p tcp --dport 25 -j ACCEPT
iptables -A OUTPUT -p tcp --dport 25 -j ACCEPT
# Mail POP3:110 
iptables -A INPUT -p tcp --dport 110 -j ACCEPT 
# Mail IMAP:143 
iptables -A INPUT -p tcp --dport 143 -j ACCEPT 
# Mail POP3S:995 
iptables -A INPUT -p tcp --dport 995 -j ACCEPT
 
 
#Pour pour le reseaux de test
#VNC
iptables -A INPUT -p tcp --dport 5900 -j ACCEPT
iptables -A OUTPUT -p tcp --dport 5900 -j ACCEPT
iptables -A INPUT -p tcp --dport 5901 -j ACCEPT
iptables -A OUTPUT -p tcp --dport 5901 -j ACCEPT
```

### Mise en place du cloud 

- branchez à eth1 côté Cloud
- vous devriez pouvoir ping 1.1.1.1

![Topologie 5](./screen/topo4.jpg)

# Configurer OpenVPN sur Rocky Linux 8

## Installation de EPEL Repository

Installation du epel-release
```
dnf install epel-release -y
```
## Installation de OpenVPN

Installation de openvpn
```
dnf install openvpn
```
## Installation de Easy-RSA

Installation de Easy-RSA
```
dnf install easy-rsa
```
## Creation de la cle publique OpenVPN

Création de la clé publique
```
mkdir /etc/easy-rsa
```
```
cp -air /usr/share/easy-rsa/3/* /etc/easy-rsa/
```
```
cd /etc/easy-rsa/
```
```
./easyrsa init-pki
```
```
./easyrsa build-ca
```
```
./easyrsa gen-dh
```
## Création des cles et du certificat pour le serveur vpn

Creation du certificat
```
cd /etc/easy-rsa
```
```
./easyrsa build-server-full server nopass
```
## Création de la cle HMAC

Générer une clé d'authentification
```
openvpn --genkey --secret /etc/easy-rsa/pki/ta.key
```
## Création certificat de revocation
```
./easyrsa gen-crl
```
```
cp -rp /etc/easy-rsa/pki/{ca.crt,dh.pem,ta.key,crl.pem,issued,private} /etc/openvpn/server/
```
## Création du certificat et de la clé client
```
cd /etc/easy-rsa
```
```
./easyrsa build-client-full client1 nopass
```
## Copie de la cle et du certificat client dans le dossier client
```
mkdir /etc/openvpn/client/{client1}
```
```
cp -rp /etc/easy-rsa/pki/{ca.crt,issued/client1.crt,private/client1.key} /etc/openvpn/client/client1
```
## Configuration du vpn

```
cp /usr/share/doc/openvpn/sample/sample-config-files/server.conf /etc/openvpn/server/
```
Cette commande permet de modifier le fichier server.conf, on pourra une fois modifier y retrouver les lignes suivantes (sans les commentaires).
```
vim /etc/openvpn/server/server.conf
```
```
port 1194
proto udp4
dev tun
ca ca.crt
cert issued/server.crt
key private/server.key  # This file should be kept secret
dh dh.pem
topology subnet
server 10.3.3.0 255.255.255.0
ifconfig-pool-persist ipp.txt
push "redirect-gateway def1 bypass-dhcp"
push "dhcp-option DNS 208.67.222.222"
push "dhcp-option DNS 192.168.10.3"
client-to-client
keepalive 10 120
tls-auth ta.key 0 # This file is secret
cipher AES-256-CBC
comp-lzo
user nobody
group nobody
persist-key
persist-tun
status /var/log/openvpn/openvpn-status.log
log-append  /var/log/openvpn/openvpn.log
verb 3
explicit-exit-notify 1
auth SHA512
```
```
mkdir /var/log/openvpn/
```
## Configuration des routes du vpn

```
echo "net.ipv4.ip_forward = 1" >> /etc/sysctl.conf
```
```
sysctl --system
```

Gestion des firewall: 

```
firewall-cmd --add-port=1194/udp --permanent
```
```
firewall-cmd --add-masquerade --permanent
```
```
ip route get 8.8.8.8
```
```
firewall-cmd --permanent --direct --passthrough ipv4 -t nat -A POSTROUTING -s 10.8.0.0/24 -o enp0s3 -j MASQUERADE
```

Comme on a utilisé des commandes --permanent on doit reload pour que les modifications prennent effet:

```
firewall-cmd --reload
```
```
systemctl enable --now openvpn-server@server
```
```
ip add s
```
```
tail /var/log/openvpn/openvpn.log
```

* Voici la Topologie avec le VPN

![Topologie 6](./screen/topo5.jpg)

# Installer et configurer un client pour le vpn

## Installation de Epel-Release et OpenVPN
```
dnf install epel-release -y
```
```
dnf install openvpn
```

Dans l'installation du vpn (voir si-dessus), vous avez généré des clés. Copiez-les sur le client et notez le chemin où elles sont stockées.

Vous devez également copier la clé HMAC et le certificat CA sur le client.

Emplacement des fichiers sur le vpn:
/etc/easy-rsa/pki/ca.crt
/etc/easy-rsa/pki/crl.pem

Vous pouvez ensuite créer la configuration du client OpenVPN.

Par exemple, pour créer un fichier de configuration OpenVPN pour le client, client1, dont les certificats et les clés sont, client1.crt et client1.key :

## Configuration du client

```
vim client1.ovpn
```
```
client
tls-client
pull
dev tun
proto udp4
remote 192.168.60.19 1194
resolv-retry infinite
nobind
#user nobody
#group nogroup
persist-key
persist-tun
key-direction 1
remote-cert-tls server
auth-nocache
comp-lzo
verb 3
auth SHA512
tls-auth ta.key 1
ca ca.crt
cert client1.crt
key client1.key
```

## Se connecter au serveur vpn

STtatus: ""
```
 openvpn-client@client1.service - OpenVPN tunnel for client1
   Loaded: loaded (/usr/lib/systemd/system/openvpn-client@.service; disabled; vendor preset: disabled)
   Active: active (running) since Wed 2021-06-30 15:48:47 EDT; 12s ago
     Docs: man:openvpn(8)
           https://community.openvpn.net/openvpn/wiki/Openvpn24ManPage
           https://community.openvpn.net/openvpn/wiki/HOWTO
 Main PID: 39782 (openvpn)
   Status: "Initialization Sequence Completed"
    Tasks: 1 (limit: 11272)
   Memory: 1.6M
   CGroup: /system.slice/system-openvpn\x2dclient.slice/openvpn-client@client1.service
           └─39782 /usr/sbin/openvpn --suppress-timestamps --nobind --config client1.conf
sudo openvpn --config client-config.ovpn
```
Si la connexion au serveur OpenVPN est réussie, vous devriez voir: Initialization Sequence Completed

# NFS : Configuration du serveur NFS

## Configuration du serveur NFS
```
dnf -y install nfs-utils 
```
```
vim /etc/idmapd.conf
```
Enlever le commentaire devant:
```
Domain = srv.world
```
```
vim /etc/exports
```
```
#create new
#for example, set [/home/nfsshare] as NFS share

/home/nfsshare 10.0.0.0/24(rw,no_root_squash)
```
```
mkdir /home/nfsshare 
```
```
systemctl enable --now rpcbind nfs-server 
```
## Autoriser le service NFS
```
firewall-cmd --add-service=nfs 
```
si vous utilisez NFSv3, autorisez ce qui suit:
```
firewall-cmd --add-service={nfs3,mountd,rpc-bind}
```
```
firewall-cmd --runtime-to-permanent
```
* Voici l'évolution de la topo avec le serveur NFS
![Topologie 6](./screen/topo6.jpg)

# Installer le serveur Apache (web)

- Paquet httpd

Pour installer apache: [greg@web ~]$ sudo dnf install httpd

- Le fichier de conf principal est /etc/httpd/conf/httpd.conf
- Je vous conseille vivement de virer tous les commentaire du fichier, à défaut de les lire, vous y verrez plus clair
- Avec vim vous pouvez tout virer avec :g/^ *#.*/d

```
[greg@web ~]$ cat /etc/httpd/conf/httpd.conf

ServerRoot "/etc/httpd"

Listen 80

[...]
```

#### Démarrer le service Apache

- Démarrez le service Apache

```
[greg@web ~]$ systemctl start httpd
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-units ====
Authentication is required to start 'httpd.service'.
Authenticating as: greg
Password:
==== AUTHENTICATION COMPLETE ====
```

- Faites en sorte qu'Apache démarre automatique au démarrage de la machine

```
[greg@web ~]$ systemctl enable httpd
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-unit-files ====
Authentication is required to manage system service or unit files.
Authenticating as: greg
Password:
==== AUTHENTICATION COMPLETE ====
==== AUTHENTICATING FOR org.freedesktop.systemd1.reload-daemon ====
Authentication is required to reload the systemd state.
Authenticating as: greg
Password:
==== AUTHENTICATION COMPLETE ====
```

- Ouvrez le port firewall nécessaire

```
[greg@web ~]$ sudo firewall-cmd --add-port=80/tcp
success
[greg@web ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
```

- Utiliser une commande ss pour savoir sur quel port tourne actuellement Apache

```
[greg@web ~]$ sudo ss -tlpn
State   Recv-Q  Send-Q   Local Address:Port    Peer Address:Port  Process
[...]
LISTEN  0       128                  *:80                 *:*      users:(("httpd",pid=1467,fd=4),("httpd",pid=1466,fd=4),("httpd",pid=1465,fd=4),("httpd",pid=1463,fd=4))
[...]
```
![Topo 7](./screen/topo7.jpg)


# Les Moindres privileges 

Le principe de moindre privilège est un principe qui stipule, selon l'ANSSI1, qu'une tâche ne doit bénéficier que de privilèges strictement nécessaires à l'exécution du code menant à bien ses fonctionnalités. En d'autres termes, une tâche ne devrait avoir la possibilité de mener à bien que les actions dont l'utilité fonctionnelle est avérée. 

* Nous avons des restrictions qui ont etaient appliqué depuis la premiere etape . Elles n'ont pas etait presenté à chaque moment mais vous pouvez suivre nos exemples juste en dessous si vous voulez vraiment faire un Zero Trust

Voici quelque exemple de moindre privilege :

> **1.** Auditer la totalité de l’environnement afin d’identifier les comptes à privilèges (mots de passe, clés SSH, codes de hachage de mots de passe ou clés d’accès) sur site, dans le cloud, dans les environnements DevOps et sur les terminaux.
  
> **2.**  Éliminer les privilèges d’administrateur local inutiles et s’assurer que tous les utilisateurs humains et non humains disposent uniquement des privilèges nécessaires pour accomplir leur travail.

> **3.** Séparer les comptes administrateur des comptes standard et isoler les sessions des utilisateurs à privilèges.

> **4.** Vous pouvez provisionner les identifiants de comptes administrateur à privilèges dans un coffre-fort numérique pour commencer à sécuriser et à gérer ces comptes.

> **5.** Renouveler immédiatement tous les mots de passe administrateur après chaque utilisation pour invalider tous les identifiants qui auraient pu être enregistrés par un keylogger et pour diminuer le risque d’attaque Pass-the-Hash.

> **6.** Superviser en continu toutes les activités liées aux comptes administrateur afin de faciliter la détection et la création d’alertes sur les activités anormales susceptibles d’indiquer qu’une attaque est en cours.

> **7.** Activer l’élévation de l’accès « juste à temps », qui permet aux utilisateurs d’accéder aux comptes à privilèges ou d’exécuter des commandes à privilèges sur une base temporaire et selon les besoins.

Si nous devons comparer cette liste avec notre tp actuel, le premier et deuxième point serait validé. En efet le package nfs permet d'effectuer les points 1 et 2 de cette installation et de proteger le systeme en question.
Nous pouvons le vérifier en exécutant  "sudo cat passwd" La machine affichera une base de données textuelle d'informations sur les utilisateurs qui peuvent se connecter au système.

# Conclusion

Le TP fonctionne correctement: les clients peuvent se connecter au serveur vpn afin d'acceder a des pages web ou autres, les connections inter-VLAN sont donc bien configurés. Le blocage de trafic configuré via les firewall ce qui permet d'augmenter la sécurité de notre infrastructure et le NFS est également activé et fonctionne grâce au script de backup.

![Topologie 8](./screen/schema.jpg)
